﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IMongoCollection<Preference> _preferenceCollection;
        private readonly IMongoCollection<Customer> _customerCollection;

        public MongoDbInitializer(IMongoDatabase mongoDb)
        {
            _preferenceCollection = mongoDb.GetCollection<Preference>(nameof(Preference));
            _customerCollection = mongoDb.GetCollection<Customer>(nameof(Customer));
        }

        public void InitializeDb()
        {
            var preferenceCount = _preferenceCollection.Find(x => x.Id == new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") ||
                                                                  x.Id == new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") ||
                                                                  x.Id == new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84")).CountDocuments();
            if (preferenceCount == 0)
            {
                _preferenceCollection.DeleteMany(e => true);
                _preferenceCollection.InsertMany(FakeDataFactory.Preferences);
            }

            var defaultcustomer = _customerCollection.Find(x => x.Id == new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0")).FirstOrDefault();
            if (defaultcustomer == null)
            {
                _customerCollection.DeleteMany(e => true);
                _customerCollection.InsertMany(FakeDataFactory.Customers);
            }
        }
    }

}